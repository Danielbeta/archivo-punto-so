

## Implementación de una libreria dinámica de formato .so para comunicacion serial entre microprocesador y python 2.7.0, para sistema operativo Raspbian.

Para ejecutar este tipo de comandos, se debe tener un sistema operativo de Linux como Devian o Ubuntu.
Es necesario tener instalado en la Raspberry el SWIG 3.0 para poder ejecutar los lineas en la consola de linux, los comandos estan en el archivo make.sh.


1. Ejecutar el archivo make.sh y tener todos los archivos generados en la misma carpeta.
2. importar desde python la libreria con la linea: import NOMBRE_DE_LA_LIBRERIA.
3. Usar la libreria desde python como si fuera una libreria normal de python.